package com.cardinalsolutions.sectioned_adapter;

import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class for generating section headers for a data set.  Each item must implement
 * {@link Categorizable} to provide the name of its category, which will be used as the title for each section.
 */
class SectionGenerator {

  public static final String TAG = SectionGenerator.class.getSimpleName();

  /**
   * Adds section header objects into a list of {@link Categorizable} data objects.  For each
   * unique category title returned by the list objects, a corresponding section header will be
   * inserted into the list.
   *
   * @param items a {@link List} of <code>Categorizable</code> objects
   * @return the original list of objects with {@link com.cardinalsolutions.sectioned_adapter.SectionedAdapter.SectionHeader}s inserted
   */

  static List<Object> getSectionsForItems(List<? extends Categorizable> items) {
    return getSectionsForItems(items, false);
  }
  static List<Object> getSectionsForItems(List<? extends Categorizable> items, boolean withFooter) {
    /* Creates a mapping of section headers to the position they should be inserted in the list */
    Map<Integer, String> sectionHeaders = new HashMap<>();

    for (int i = 0; i < items.size(); i++) {
      String category = items.get(i).getCategory();
      if (!sectionHeaders.containsValue(category)) {
        sectionHeaders.put(i + sectionHeaders.size(), category);
      }
    }

    /* Merges the list of section headers into the appropriate position in the object list */
    ArrayList<Object> listItems = new ArrayList<>();
    int sectionCount = 0;

    List<Integer> sectionHeaderIndexes = new ArrayList<>();

    for (int i = 0; i < items.size() + sectionHeaders.size(); i++) {
      if (sectionHeaders.containsKey(i)) {
        listItems.add(new SectionedAdapter.SectionHeader(sectionHeaders.get(i)));
        sectionCount++;
        sectionHeaderIndexes.add(i);
      } else {
        listItems.add(items.get(i - sectionCount));
      }
    }

    if(withFooter) {
      Map<Integer, String> sectionFooters = new HashMap<>();

      for (int i = 0; i < items.size(); i++) {
        String footer = items.get(i).getFooter();
        if (!sectionFooters.containsValue(footer)) {
          sectionFooters.put(i + sectionFooters.size(), footer);
        }
      }

      int count = 0;
      for (int i = 0; i < sectionHeaderIndexes.size() - 1; i++) {
        int idx = sectionHeaderIndexes.get(i + 1);

        int itemKey = sectionHeaderIndexes.get(i);
        String footer = sectionFooters.get(itemKey);
        listItems.add(count + idx, new SectionedAdapter.SectionFooter(footer));
        count++;
      }

      // add last item
      String footer = sectionFooters.get(sectionHeaderIndexes.get(sectionHeaderIndexes.size() - 1));
      listItems.add(new SectionedAdapter.SectionFooter(footer));
    }
    return listItems;
  }
}
